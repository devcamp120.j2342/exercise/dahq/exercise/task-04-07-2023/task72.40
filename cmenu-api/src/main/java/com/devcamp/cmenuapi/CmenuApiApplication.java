package com.devcamp.cmenuapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CmenuApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(CmenuApiApplication.class, args);
	}

}
