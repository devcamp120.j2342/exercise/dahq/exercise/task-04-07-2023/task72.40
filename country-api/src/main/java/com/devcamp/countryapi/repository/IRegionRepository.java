package com.devcamp.countryapi.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.countryapi.model.CRegion;
import java.util.List;

public interface IRegionRepository extends JpaRepository<CRegion, Long> {
    List<CRegion> findByCountryId(long id);

    long countByCountryId(long id);

}
